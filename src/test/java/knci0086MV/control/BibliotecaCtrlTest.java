package knci0086MV.control;

import knci0086MV.model.Carte;
import org.junit.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class BibliotecaCtrlTest {
    Carte C1,C2,C3,C4,C5,C6,C7,C8;
    BibliotecaCtrl ctrl;


    @Before
    public void setUp() throws Exception {

        List<String> autoriC1 = new ArrayList<>(Arrays.asList("Amita Bhose"));
        List<String> cuvinteCheieC1 = new ArrayList<>(Arrays.asList("Junimea", "Amita", "Bhose"));

        List<String> autoriC2 = new ArrayList<>(Arrays.asList("Gheorghe Bulgăr"));
        List<String> cuvinteCheieC2 = new ArrayList<>(Arrays.asList("Eminescu", "Minerva"));

        List<String> autoriC3 = new ArrayList<>(Arrays.asList("Constantin I. Calotă"));
        List<String> cuvinteCheieC3 = new ArrayList<>(Arrays.asList("Constantin", "Calotă"));

        List<String> autoriC4 = new ArrayList<>(Arrays.asList("George Calinescu"));
        List<String> cuvinteCheieC4 = new ArrayList<>(Arrays.asList("George", "Calinescu"));

        List<String> autoriC5 = new ArrayList<>(Arrays.asList("Amita Bhose"));
        List<String> cuvinteCheieC5 = new ArrayList<>(Arrays.asList("Junimea", "Amita", "Bhose"));

        List<String> autoriC6 = new ArrayList<>(Arrays.asList("Constantin I. Calotă"));
        List<String> cuvinteCheieC6 = new ArrayList<>(Arrays.asList("Constantin", "Calotă"));

        List<String> autoriC7 = new ArrayList<>(Arrays.asList("George Calinescu"));
        List<String> cuvinteCheieC7 = new ArrayList<>(Arrays.asList("George", "Calinescu"));

        List<String> autoriC8 = new ArrayList<>(Arrays.asList("George Calinescu"));
        List<String> cuvinteCheieC8 = new ArrayList<>(Arrays.asList("George", "Calinescu"));

        C1= new Carte("MIjloc",autoriC1, "1918", "Junimea1234",cuvinteCheieC1);
        C2= new Carte("Literatura Romana",autoriC2, "1971", "Minerva1234",cuvinteCheieC2);
        C3= new Carte("psiho romantismului",autoriC3, "1936", "Editura Librăriei, Bucuresti",cuvinteCheieC3);
        C4= new Carte("Opera lui Mihai Eminescu",autoriC4, "1788", "Minerva2009",cuvinteCheieC4);
        C5= new Carte("Eminescu1",autoriC5, "1978", "Junimea1234",cuvinteCheieC5);
        C6= new Carte("Poezia lui Mihail Eminescu și psihologia romantismului",autoriC6, "1936", "Editur",cuvinteCheieC6);
        C7= new Carte("Opera lui Mihai Eminescu",autoriC7, "1800", "Minerva2015",cuvinteCheieC7);
        C8= new Carte("Opera lui Mihai Eminescu",autoriC8, "2019", "Minerva2015",cuvinteCheieC8);
        System.out.println("in BeforeTest");
    }

    @After
    public void tearDown() throws Exception {
        C1 = null;
        C2 = null;
        C3 = null;
        C4 = null;
        C5 = null;
        C6 = null;
        C7 = null;
        C8 = null;
        System.out.println("in AfterTest");
    }

    @Test (expected=Exception.class)
    public void adaugaCarte1() throws Exception {
        ctrl = new BibliotecaCtrl();
        ctrl.adaugaCarte(C1);
    }
    @Test (expected=Exception.class)
    public void adaugaCarte2() throws Exception {
        ctrl = new BibliotecaCtrl();
        ctrl.adaugaCarte(C2);
    }
    @Test(expected=Exception.class)
    public void adaugaCarte3() throws Exception {
       ctrl.adaugaCarte(C3);
    }
    @Test(expected=Exception.class)
    public void adaugaCarte4() throws Exception {
        ctrl.adaugaCarte(C4);
    }
    @Test(expected=Exception.class)
    public void adaugaCarte5() throws Exception {
        ctrl.adaugaCarte(C5);
    }
    @Test(expected=Exception.class)
    public void adaugaCarte6() throws Exception {
        ctrl.adaugaCarte(C6);
    }
    @Test(expected=Exception.class)
    public void adaugaCarte7() throws Exception {
        ctrl.adaugaCarte(C7);
    }
    @Test(expected=Exception.class)
    public void adaugaCarte8() throws Exception {
        ctrl.adaugaCarte(C8);
    }

}